﻿namespace ProcessDocuments
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcessFolder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolderToProcess = new System.Windows.Forms.TextBox();
            this.btnBrowseForFolder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.btnBrowseForOutputFolder = new System.Windows.Forms.Button();
            this.conversionProgress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // btnProcessFolder
            // 
            this.btnProcessFolder.Location = new System.Drawing.Point(529, 107);
            this.btnProcessFolder.Name = "btnProcessFolder";
            this.btnProcessFolder.Size = new System.Drawing.Size(112, 23);
            this.btnProcessFolder.TabIndex = 0;
            this.btnProcessFolder.Text = "Process Folder";
            this.btnProcessFolder.UseVisualStyleBackColor = true;
            this.btnProcessFolder.Click += new System.EventHandler(this.btnProcessFolder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Source folder";
            // 
            // txtFolderToProcess
            // 
            this.txtFolderToProcess.Location = new System.Drawing.Point(12, 69);
            this.txtFolderToProcess.Name = "txtFolderToProcess";
            this.txtFolderToProcess.Size = new System.Drawing.Size(584, 20);
            this.txtFolderToProcess.TabIndex = 2;
            this.txtFolderToProcess.Text = "C:\\GS01";
            this.txtFolderToProcess.TextChanged += new System.EventHandler(this.txtFolderToProcess_TextChanged);
            // 
            // btnBrowseForFolder
            // 
            this.btnBrowseForFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseForFolder.Location = new System.Drawing.Point(602, 67);
            this.btnBrowseForFolder.Name = "btnBrowseForFolder";
            this.btnBrowseForFolder.Size = new System.Drawing.Size(39, 23);
            this.btnBrowseForFolder.TabIndex = 3;
            this.btnBrowseForFolder.Text = "...";
            this.btnBrowseForFolder.UseVisualStyleBackColor = true;
            this.btnBrowseForFolder.Click += new System.EventHandler(this.btnBrowseForFolder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Output folder";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(12, 24);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(584, 20);
            this.txtOutputFolder.TabIndex = 5;
            this.txtOutputFolder.Text = "C:\\GS01\\Output";
            // 
            // btnBrowseForOutputFolder
            // 
            this.btnBrowseForOutputFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseForOutputFolder.Location = new System.Drawing.Point(602, 22);
            this.btnBrowseForOutputFolder.Name = "btnBrowseForOutputFolder";
            this.btnBrowseForOutputFolder.Size = new System.Drawing.Size(39, 23);
            this.btnBrowseForOutputFolder.TabIndex = 6;
            this.btnBrowseForOutputFolder.Text = "...";
            this.btnBrowseForOutputFolder.UseVisualStyleBackColor = true;
            this.btnBrowseForOutputFolder.Click += new System.EventHandler(this.btnBrowseForOutputFolder_Click);
            // 
            // conversionProgress
            // 
            this.conversionProgress.Location = new System.Drawing.Point(13, 109);
            this.conversionProgress.Name = "conversionProgress";
            this.conversionProgress.Size = new System.Drawing.Size(508, 21);
            this.conversionProgress.TabIndex = 7;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 140);
            this.Controls.Add(this.conversionProgress);
            this.Controls.Add(this.btnBrowseForOutputFolder);
            this.Controls.Add(this.txtOutputFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBrowseForFolder);
            this.Controls.Add(this.txtFolderToProcess);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnProcessFolder);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnProcessFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFolderToProcess;
        private System.Windows.Forms.Button btnBrowseForFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Button btnBrowseForOutputFolder;
        private System.Windows.Forms.ProgressBar conversionProgress;
    }
}

