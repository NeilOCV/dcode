﻿using System;
using System.IO;
using System.Windows.Forms;

namespace ProcessDocuments
{
    public partial class frmMain : Form
    {
        ProcessDocuments _processDocuments;
        public frmMain()
        {
            InitializeComponent();
            txtFolderToProcess.Text = Properties.Settings.Default["SourceFolder"].ToString();
            txtOutputFolder.Text = Properties.Settings.Default["OutputFolder"].ToString();
            _processDocuments = new ProcessDocuments();
            _processDocuments.Progress += _processDocuments_Progress;
        }

        private void _processDocuments_Progress(object sender, ProgressEventArgs args)
        {
            conversionProgress.Maximum = args.TotalNumberToConvert;
            conversionProgress.Value = args.ConvertedCount;
        }

        private void KickOffProcess()
        {
            Properties.Settings.Default["SourceFolder"] = txtFolderToProcess.Text;
            Properties.Settings.Default["OutputFolder"] = txtOutputFolder.Text;
            Properties.Settings.Default.Save();
            _processDocuments.ConvertDocuments(txtFolderToProcess.Text, txtOutputFolder.Text);
        }

        private void btnProcessFolder_Click(object sender, System.EventArgs e)
        {
            KickOffProcess();
        }

        private void txtFolderToProcess_TextChanged(object sender, System.EventArgs e)
        {
            btnProcessFolder.Enabled = txtFolderToProcess.Text != "";
        }

        private void btnBrowseForFolder_Click(object sender, System.EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtFolderToProcess.Text = fbd.SelectedPath;
                }
            }
        }

        private void btnBrowseForOutputFolder_Click(object sender, System.EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtOutputFolder.Text = fbd.SelectedPath;
                }
            }
        }
    }
}
