﻿using System;

namespace ProcessDocuments
{
    public class ProgressEventArgs : EventArgs
    {
        public int TotalNumberToConvert { get; set; } = 0;
        public int ConvertedCount { get; set; } = 0;
    }
}
