﻿using System;

namespace ProcessDocuments.Models
{
	public class WorkInstructionTextItem
	{
		public Guid WorkInstructionItemId { get; set; } = Guid.NewGuid();

		/// <summary>
		/// Items are grouped by <see cref="P:dCode.Models.PlantMaintenance.WorkInstructionTextItem.GroupName" /> when displayed as a Survey
		/// </summary>
		public string GroupName { get; set; }

		public string Text { get; set; }

		public string SubText { get; set; }
	}
}
