﻿using System;

namespace ProcessDocuments.Models
{
	public class Attachment
	{
		public string Title { get; set; }

		public byte[] FileBytes { get; set; }

		public string FileName { get; set; }

		public string MimeType { get; set; }

		public string Path { get; set; }

		public string ExternalId { get; set; }

		public long Size { get; set; }

		public DateTime CreatedDate { get; set; }
	}
}
