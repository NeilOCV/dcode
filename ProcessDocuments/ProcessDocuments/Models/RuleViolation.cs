﻿namespace ProcessDocuments.Models
{
	public class RuleViolation
	{
		public string Rule { get; set; }
		public string Message { get; set; }
		public bool IsCritical { get; set; }
	}
}
