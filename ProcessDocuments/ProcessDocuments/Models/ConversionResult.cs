﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ProcessDocuments.Models
{
	public class ConversionResult
	{
		public string Filename;

		public int ConversionScore;

		private List<RuleViolation> _ruleViolations = new List<RuleViolation>();

		public bool Aborted => _ruleViolations.Any((RuleViolation t) => t.IsCritical);

		public bool HasRuleViolations => _ruleViolations.Count > 0;

		public ReadOnlyCollection<RuleViolation> RuleViolations => new ReadOnlyCollection<RuleViolation>(_ruleViolations);

		public WorkInstruction WorkInstructions { get; set; } = new WorkInstruction();

		public void AddRuleViolation(string ruleName, bool abort, string warningText)
		{
			_ruleViolations.Add(new RuleViolation
			{
				Message = warningText,
				IsCritical = abort,
				Rule = ruleName
			});
		}
	}
}
