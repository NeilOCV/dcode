﻿using System.Collections.Generic;

namespace ProcessDocuments.Models
{
	public class WorkInstruction
	{
		public List<WorkInstructionTextItem> InstructionsAsText { get; set; }
		public string SourceFilename { get; set; }
	}
}
