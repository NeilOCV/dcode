﻿using Newtonsoft.Json;
using ProcessDocuments.ExtensionMethods;
using System;
using System.IO;
using System.Linq;

namespace ProcessDocuments
{
	public class ProcessDocuments
    {
		public delegate void ProgressEventHandler(object sender, ProgressEventArgs args);
		public event ProgressEventHandler Progress;
		public void ConvertDocuments(string InputPath, string OutputPath)
		{
			if (string.IsNullOrEmpty(OutputPath)) OutputPath = InputPath;
			var fileEntries = Directory.EnumerateFiles(InputPath, "*.*", SearchOption.TopDirectoryOnly).Where(s => s.EndsWith(".doc") || s.EndsWith(".docx"));
			try
			{
				Directory.CreateDirectory(OutputPath);  //Just to make sure the output folder is available
			}
			catch (Exception ex)
			{
				throw;
			}
			int progressCounter = 0;
			foreach (string filePath in fileEntries)
			{
				var json = filePath.ExtractWorkInstructions();
				File.WriteAllText(OutputPath + @"\" + Path.GetFileName(filePath) + ".result.json", JsonConvert.SerializeObject(json, Formatting.Indented));
				progressCounter++;
				OnProgress(fileEntries.Count(), progressCounter);
			}
		}
		
		protected virtual void OnProgress(int TotalToBeConverted, int ConvertedCount)
		{
			if (Progress != null)
				Progress(this, new ProgressEventArgs() { ConvertedCount = ConvertedCount, TotalNumberToConvert = TotalToBeConverted });
		}
	}
}
