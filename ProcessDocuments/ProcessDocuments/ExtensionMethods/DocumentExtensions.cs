﻿using Aspose.Words;
using ProcessDocuments.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProcessDocuments.ExtensionMethods
{
    static public class DocumentExtensions
    {
        private static WorkInstruction ConvertToInstruction(string FileName, List<string> Instructions, string GroupName)
        {
            WorkInstruction result = new WorkInstruction() { SourceFilename = FileName, InstructionsAsText = new List<WorkInstructionTextItem>() };

            for (int counter=0; counter < Instructions.Count(); counter++)
            {
                WorkInstructionTextItem item = new WorkInstructionTextItem();
                item.SubText = "";
                item.GroupName = GroupName;
                item.Text = Instructions[counter];
                result.InstructionsAsText.Add(item);
            }
            return result;
        }
        public static WorkInstruction ExtractWorkInstructions(this string filePath)
        {
            var tempFile = filePath + ".temp.txt";
            var documentFile = new Document(filePath);
            documentFile.Save(tempFile);

            var lines = File.ReadAllLines(tempFile).ToList();

            var originalLines = lines.ToArray();

            var firstWorkItemSitsHere = 0;

            var groupName = "";

            for(int counter = 0; counter < lines.Count; counter++)
            {
                var inspectThisLine = lines[counter];
                if (inspectThisLine.ToUpper() == ("Work Instruction".ToUpper()) || (inspectThisLine.ToUpper() == ("SRINKLER CONTROL VALVES".ToUpper())))
                {
                    break;
                }
                else
                {
                    lines[counter] = "";
                }
            }

            var instructions = new List<string>();

            for (int counter = 1; counter < 10000; counter++)
            {
                var line = (from list in lines where list.StartsWith($"{counter}. ") && list.Length > 5 select list).FirstOrDefault();
                if (line != null) if (line.Any())
                    {
                        if (counter == 1) firstWorkItemSitsHere = lines.IndexOf(line);
                        instructions.Add(line);
                    }
                    else
                    {
                        break;
                    }
            }
            for (int counter = firstWorkItemSitsHere - 1; counter > 0; counter--)
            {
                var inspectThisLine = lines[counter];
                if (!string.IsNullOrEmpty(inspectThisLine))
                {
                    groupName = inspectThisLine;
                    break;
                }
            }
            File.Delete(tempFile);
            return ConvertToInstruction(filePath, instructions, groupName);
        }
    }
}
